import { Component, OnInit } from '@angular/core';
import { SwapiGetService } from '../services/swapi-get.service';

@Component({
  selector: 'app-swapi-entity-list',
  templateUrl: './swapi-entity-list.component.html',
  styleUrls: ['./swapi-entity-list.component.css']
})
export class SwapiEntityListComponent implements OnInit {

  // Slightly hacky - allow looping over keys
  Object = Object;

  swapiEntityTypes = ['people', 'planets', 'films', 'species', 'vehicles', 'starships'];
  currentlySelectedTypeIndex = 0;

  swapiEntityList: object;
  currentlySelectedItem: number;
  currentPage = 1;

  constructor(private swapiGetService: SwapiGetService) { }

  getSwapiEntityList(entityTypeIndex: number) {
    this.currentlySelectedTypeIndex = entityTypeIndex;
    this.swapiGetService.getEntities(this.swapiEntityTypes[entityTypeIndex], this.currentPage).subscribe(
      entityList => {
        console.log(entityList);
        this.swapiEntityList = entityList;
        this.currentlySelectedItem = 0;
      },
      error => {
        console.log('Failed to retrieve type [' + entityTypeIndex + ']');
        console.log(error);
      }
    );
  }

  changeEntityType(event) {
    this.currentPage = 1;
    this.getSwapiEntityList(event.target.selectedIndex);
  }

  changeEntityItem(event) {
    this.currentlySelectedItem = event.target.selectedIndex;
  }

  nextPage() {
    ++this.currentPage;
    this.refreshEntityList();
  }

  prevPage() {
    if (this.currentPage > 1) {
      --this.currentPage;
      this.refreshEntityList();
    }
  }

  refreshEntityList() {
    this.getSwapiEntityList(this.currentlySelectedTypeIndex);
  }

  ngOnInit() {
    this.refreshEntityList();
  }

}
