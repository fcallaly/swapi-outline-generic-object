import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwapiEntityListComponent } from './swapi-entity-list.component';

describe('SwapiEntityListComponent', () => {
  let component: SwapiEntityListComponent;
  let fixture: ComponentFixture<SwapiEntityListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwapiEntityListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwapiEntityListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
