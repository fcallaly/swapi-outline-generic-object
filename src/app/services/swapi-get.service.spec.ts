import { TestBed } from '@angular/core/testing';

import { SwapiGetService } from './swapi-get.service';

describe('SwapiGetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SwapiGetService = TestBed.get(SwapiGetService);
    expect(service).toBeTruthy();
  });
});
