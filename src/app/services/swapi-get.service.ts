import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SwapiGetService {

  constructor(private httpClient: HttpClient) { }

  getEntities(entityType: string, pageNum: number): Observable<object> {
    return this.httpClient.get<object>(environment.swapiApiUrl + entityType + '/?page=' + pageNum);
  }
}
